#!/usr/bin/env python
# -*- coding: utf-8 -*-

from six.moves import urllib
from datetime import datetime, timedelta
from mastodon import Mastodon
import unidecode
import time
import re
import os
import time
import sys
import os.path
import psycopg2
import wikipedia

from decimal import *
getcontext().prec = 2

def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext

def unescape(s):
    s = s.replace("&apos;", "'")
    return s

###############################################################################
# INITIALISATION
###############################################################################

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

###############################################################################
# main

if __name__ == '__main__':

    # Load secrets from secrets file
    secrets_filepath = "secrets/secrets.txt"
    uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
    uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
    uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

    # Load configuration from config file
    config_filepath = "config/config.txt"
    mastodon_hostname = get_parameter("mastodon_hostname", config_filepath)
    bot_username = get_parameter("bot_username", config_filepath)

    # Load Wikipedia's language from config file
    lang_config_filepath = "config/lang_config.txt"
    wiki_lang = get_parameter("wiki_lang", lang_config_filepath)

    # Load DB configuration from config file
    config_filepath = "config/db_config.txt"
    mastodon_db = get_parameter("mastodon_db", config_filepath)
    mastodon_db_user = get_parameter("mastodon_db_user", config_filepath)
    wikipedia_db = get_parameter("wikipedia_db", config_filepath)
    wikipedia_db_user = get_parameter("wikipedia_db_user", config_filepath)

    # Initialise Mastodon API
    mastodon = Mastodon(
        client_id = uc_client_id,
        client_secret = uc_client_secret,
        access_token = uc_access_token,
        api_base_url = 'https://' + mastodon_hostname,
    )

    # Initialise access headers
    headers={ 'Authorization': 'Bearer %s'%uc_access_token }

    #############################################################

    html_escape_table = {
        "&": "&amp;",
        '"': "&quot;",
        "'": "&apos;",
        ">": "&gt;",
        "<": "&lt;",
        }

    #############################################################

    if wiki_lang == "ca":

        language_filepath = "ca.txt"

    elif wiki_lang == "en":

        language_filepath = "en.txt"

    else:

        print("\nOnly 'ca' and 'en' languages are supported.\n")
        sys.exit(0)

    toot_text1 = get_parameter("toot_text1", language_filepath)
    toot_text2 = get_parameter("toot_text2", language_filepath)
    toot_text3 = get_parameter("toot_text3", language_filepath)
    toot_text4 = get_parameter("toot_text4", language_filepath)
    toot_text5 = get_parameter("toot_text5", language_filepath)
    toot_text6 = get_parameter("toot_text6", language_filepath)
    toot_text7 = get_parameter("toot_text7", language_filepath)
    toot_text8 = get_parameter("toot_text8", language_filepath)
    toot_text9 = get_parameter("toot_text9", language_filepath)

    now = datetime.now()

    attended = 0
    ambiguous = 0
    nonexistent = 0
    ignored = 0
    queries = 0

    attended_perc = 0.00
    ambiguous_perc = 0.00
    ignored_perc = 0.00
    nonexistend_perc = 0.00

    ###################################################################
    # query status_created_at of last notification

    try:

        conn = None

        conn = psycopg2.connect(database = wikipedia_db, user = wikipedia_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute("select status_created_at from queries order by id desc limit 1")

        row = cur.fetchone()
        if row != None:
            last_posted = row[0]
            last_posted = last_posted.strftime("%d/%m/%Y, %H:%M:%S")
        else:
            last_posted = ""

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

    ###################################################################################################################################
    # get bot_id from bot's username

    try:

        conn = None

        conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute("select id from accounts where username = (%s) and domain is null", (bot_username,))

        row = cur.fetchone()

        if row != None:

            bot_id = row[0]

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

    #############################################################################################################################
    # check if any new notifications by comparing newest notification datetime with the last query datetime

    last_notifications = [] # to store last 20 'Mention' type notitifications for our bot

    try:

        conn = None

        conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute("select * from notifications where activity_type = 'Mention' and account_id = (%s) order by created_at desc limit 1", (bot_id,))

        row = cur.fetchone()

        if row != None:

            last_notif_created_at = row[3]

            time_diff = now - last_notif_created_at

            if time_diff > timedelta(minutes=125):

                cur.close()

                conn.close()

                print("No new notifications")

                sys.exit(0)

            last_notif_created_at = last_notif_created_at + timedelta(hours=2)

            last_notif_datetime = last_notif_created_at.strftime("%d/%m/%Y, %H:%M:%S")

            if last_posted != "":

                if last_notif_datetime == last_posted:

                    cur.close()

                    conn.close()

                    print("No new notifications")

                    sys.exit(0)

        cur.execute("select * from notifications where activity_type = 'Mention' and account_id = (%s) order by created_at desc limit 20", (bot_id,))

        rows = cur.fetchall()
        if rows != None:

            for row in rows:

                last_notif_created_at = row[3]

                time_diff = now - last_notif_created_at

                if time_diff > timedelta(minutes=125): # only get last 5 minutes mentions

                    cur.close()

                    conn.close()

                    break

                else:

                   last_notifications.append(row)

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

    ###################################################################
    # get the totals
    ###################################################################

    try:

        conn = None
        conn = psycopg2.connect(database=wikipedia_db, user=wikipedia_db_user, password="", host="/var/run/postgresql", port="5432")

        cur = conn.cursor()

        cur.execute("select attended, ambiguous, nonexistent, ignored, total_queries from queries order by datetime desc limit 1")

        row = cur.fetchone()
        if row != None:
            attended = row[0]
            ambiguous = row[1]
            nonexistent = row[2]
            ignored = row[3]
            queries = row[4]

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:
            conn.close()

    ####################################################################

    i = 0
    while i < len(last_notifications):

        user_id = last_notifications[i][5]
        activity_id = last_notifications[i][0]
        n_created_at = last_notifications[i][3]

        n_created_at = n_created_at + timedelta(hours=2)


        n_created_datetime = n_created_at.strftime("%d/%m/%Y, %H:%M:%S")

        if last_posted != "":

            if n_created_datetime == last_posted:

                print("No more notifications")

                sys.exit(0)

        try:

            conn = None

            conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

            cur = conn.cursor()

            cur.execute("select username, domain from accounts where id=(%s)", (user_id,))

            row = cur.fetchone()

            if row != None:

                username = row[0]
                domain = row[1]

            cur.execute("select status_id from mentions where id = (%s)", (activity_id,))

            row = cur.fetchone()

            if row != None:

                status_id = row[0]

            cur.execute("select text, visibility from statuses where id = (%s)", (status_id,))

            row = cur.fetchone()

            if row != None:

                text = row[0]
                visibility = row[1]

            cur.close()

            if visibility == 0:
                visibility = 'public'
            elif visibility == 1:
                visibility = 'unlisted'
            elif visibility == 2:
                visibility = 'private'
            elif visibility == 3:
                visibility = 'direct'

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

        ignore = False

        if domain != None:

            i += 1

        try:

            conn = None
            conn = psycopg2.connect(database = wikipedia_db, user = wikipedia_db_user, password = "", host = "/var/run/postgresql", port = "5432")

            cur = conn.cursor()

            ### check if already replied

            cur.execute("SELECT id, username FROM queries where id=(%s)", (status_id,))

            row = cur.fetchone()

            if row == None:

                content = cleanhtml(text)
                content = unescape(content)

                try:

                    start = content.index("@")
                    end = content.index(" ")
                    if len(content) > end:

                        content = content[0: start:] + content[end +1::]

                    cleanit = content.count('@')

                    i = 0
                    while i < cleanit :

                        start = content.rfind("@")
                        end = len(content)
                        content = content[0: start:] + content[end +1::]
                        i += 1

                    content = content.lower()
                    question = content

                    if wiki_lang == 'ca':

                        keyword_length = 9
                        keyword = 'consulta:'

                    elif wiki_lang == 'en':

                        keyword_length = 6
                        keyword = 'query:'

                    if unidecode.unidecode(question)[0:keyword_length] == keyword:

                        wikipedia.set_lang(wiki_lang)

                        start = 0
                        end = unidecode.unidecode(question).index(keyword, 0)
                        if len(question) > end:
                            question = question[0: start:] + question[end + keyword_length+1::]

                        wikierror = 0

                        try:

                            wiki_result = wikipedia.page(question)

                        except wikipedia.exceptions.DisambiguationError as error:

                            wikierror = 1

                            wiki_result = wikipedia.search(question, results=10)

                            ambiguous_reply = "\n- ".join(str(x) for x in wiki_result)
                            if domain != None:
                                toot_text = '@' + username + '@' + domain + ' ' + question + ' ' +  toot_text1 + '\n\n- ' + ambiguous_reply
                            else:
                                toot_text = '@' + username + ' ' + question + ' ' + toot_text1 + '\n\n- ' + ambiguous_reply
                            toot_text = (toot_text[:400] + '... ') if len(toot_text) > 400 else toot_text

                            notif_type = "ambiguous"
                            answered = True
                            ambiguous += 1
                            queries += 1
                            attended_perc = (attended * 100.00) / queries
                            ambiguous_perc = (ambiguous * 100.00) / queries
                            ignored_perc = (ignored * 100.00) / queries
                            nonexistend_perc = (nonexistent * 100.00) / queries

                        except wikipedia.exceptions.PageError as page_error:

                            wikierror = 1
                            if domain != None:
                                toot_text = '@' + username + '@' + domain + " " + toot_text2 + question + toot_text3
                            else:
                                toot_text = '@' + username + " " + "l'article '" + question + toot_text3
                            toot_text += ":viqui:"
                            notif_type = "nonexistent"
                            answered = True
                            nonexistent += 1
                            queries += 1
                            attended_perc = (attended * 100.00) / queries
                            ambiguous_perc = (ambiguous * 100.00) / queries
                            ignored_perc = (ignored * 100.00) / queries
                            nonexistend_perc = (nonexistent * 100.00) / queries

                        if wikierror == 0:

                            if domain != None:
                                toot_text = '@' + username + '@' + domain + " " + "\n"
                            else:
                                toot_text = '@' + username + " " + "\n"
                            toot_text += wikipedia.summary(question, sentences=2) + "\n"
                            toot_text = (toot_text[:400] + '... ') if len(toot_text) > 400 else toot_text
                            toot_text += str(wiki_result.url) + "\n"
                            toot_text += ":viqui:"

                            notif_type = "attended"
                            answered = True
                            attended += 1
                            queries += 1
                            attended_perc = (attended * 100.00) / queries
                            ambiguous_perc = (ambiguous * 100.00) / queries
                            ignored_perc = (ignored * 100.00) / queries
                            nonexistend_perc = (nonexistent * 100.00) / queries

                    elif unidecode.unidecode(question)[0:4] == "info":

                        try:

                            conn = None

                            conn = psycopg2.connect(database = wikipedia_db, user = wikipedia_db_user, password = "", host = "/var/run/postgresql", port = "5432")

                            cur = conn.cursor()

                            cur.execute("select attended, ambiguous, nonexistent, ignored, total_queries, attended_perc, ambiguous_perc, nonexistent_perc, ignored_perc from queries order by datetime desc limit 1")

                            row = cur.fetchone()

                            if row != None:

                                attended = row[0]
                                ambiguous = row[1]
                                nonexistent = row[2]
                                queries = row[4]
                                attended_perc = row[5]
                                ambiguous_perc = row[6]
                                nonexistend_perc = row[7]
                                ignored_perc = row[8]

                            cur.close()

                            if domain != None:
                                toot_text = '@' + username + '@' + domain + " " + "\n"
                            else:
                                toot_text = '@' + username + " " + "\n"
                            toot_text += "\n"
                            toot_text += toot_text4 + "\n"
                            toot_text += "\n"
                            toot_text += toot_text5 + ' ' + str(attended) +" \n"
                            toot_text += toot_text6 + ' ' + str(ambiguous) +" \n"
                            toot_text += toot_text7 + ' ' + str(nonexistent) +" \n"
                            toot_text += toot_text8 + ' ' + str(ignored) +" \n"
                            toot_text += "\n"
                            toot_text += toot_text9 + ' ' + str(queries)
                            notif_type = "info"
                            answered = True

                        except (Exception, psycopg2.DatabaseError) as error:

                            print(error)

                        finally:

                            if conn is not None:

                                conn.close()

                    else:

                        notif_type = "ignored"
                        ignore = True
                        answered = False
                        ignored += 1
                        queries += 1
                        question = (question[:95] + '... ') if len(question) > 99 else question
                        attended_perc = (attended * 100.00) / queries
                        ambiguous_perc = (ambiguous * 100.00) / queries
                        ignored_perc = (ignored * 100.00) / queries
                        nonexistend_perc = (nonexistent * 100.00) / queries

                    if answered == True:

                        if ignore == False:

                            mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility=visibility )
                            print("Tooting")

                            ################################################################################################################################
                            # write the query to database

                            insert_query = """INSERT INTO queries(datetime, id, username, replied, notif_type, status_created_at, attended, ambiguous, nonexistent, ignored, total_queries,
                                              query, attended_perc, ambiguous_perc, nonexistent_perc, ignored_perc) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""

                            conn = None

                            try:

                                conn = psycopg2.connect(database = wikipedia_db, user = wikipedia_db_user, password = "", host = "/var/run/postgresql", port = "5432")

                                cur = conn.cursor()

                                cur.execute(insert_query, (now, status_id, username, answered, notif_type, last_notif_created_at, attended, ambiguous, nonexistent, ignored, queries, question, attended_perc, ambiguous_perc, nonexistend_perc, ignored_perc))

                                conn.commit()

                                cur.close()

                            except (Exception, psycopg2.DatabaseError) as error:

                                print(error)

                            finally:

                                if conn is not None:

                                    conn.close()

                            ################################################################################################################################

                            i += 1

                        else:

                            break

                    if answered == False:

                        if ignore == True:

                            ################################################################################################################################
                            # write the ignored query to database

                            insert_query = """INSERT INTO queries(datetime, id, username, replied, notif_type, status_created_at, attended, ambiguous, nonexistent, ignored, total_queries,
                                              query, attended_perc, ambiguous_perc, nonexistent_perc, ignored_perc) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
                            conn = None

                            try:

                                conn = psycopg2.connect(database = wikipedia_db, user = wikipedia_db_user, password = "", host = "/var/run/postgresql", port = "5432")

                                cur = conn.cursor()

                                cur.execute(insert_query, (now, status_id, username, answered, notif_type, n_created_at, attended, ambiguous, nonexistent, ignored, queries, question, attended_perc, ambiguous_perc, nonexistend_perc, ignored_perc))

                                conn.commit()

                                cur.close()

                            except (Exception, psycopg2.DatabaseError) as error:

                                print(error)

                            finally:

                                if conn is not None:

                                    conn.close()
                            i += 1

                except ValueError as v_error:

                    print(v_error)

                    cur.close()

            else:

                i += 1

        except (Exception, psycopg2.DatabaseError) as error:

            sys.exit(error)

        finally:

            if conn is not None:

                conn.close()

